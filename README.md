# Group Policies

**Create Batch Users Script**

```PowerShell
for ($i = 1; $i -le 500; $i++) {
    New-ADUser -Name "user$i" -DisplayName "user$i" -AccountPassword (ConvertTo-SecureString "123+qwe" -AsPlainText -Force) -Enabled $true -ChangePasswordAtLogon $False –PasswordNeverExpires $true
}
```

---

**Password**

- Computer Configuration
    - Policies
        - Windows Settings
            - Security Settings
                - Account Policies
                    - Password Policy
                        - *Minimum password length :::: Password must meet complexity requirements*

---

**Disable CTRL+ALT+DEL**

- Computer Configuration
    - Policies
        - Windows Settings
            - Security Settings
                - Local Policies
                    - Security Options
                        - *IL: Do not require CTRL+ALT+DEL*

---

**Warning at Logon**

- Computer Configuration
    - Policies
        - Windows Settings
            - Security Settings
                - Local Policies
                    - Security Options
                        - *Interactive Logon*

---

**Don't show name of last user who logged in**

- Computer Configuration
    - Policies
        - Windows Settings
            - Security Settings
                - Local Policies
                    - Security Options
                        - *Interactive Logon*

--- 

**Prohibit Access To Control Panel**

- User Configuration
    - Policies
        - Administrative Templates
             - Control Panel
                - *Prohibit access to Control Panel and PC settings*

--- 

**No CMD**

- User Configuration
    - Policies
        - Administrative Templates
            - System
                - *Prevent access to the command prompt*

--- 

**No RUN**

- User Configuration
    - Policies
        - Administrative Templates
            - Start Menu and Taskbar
                - *Remove Run menu from Start Menu*

---

**Same Wallpaper**

- User Configuration
    - Policies
        - Administrative Templates
            - Desktop
                - Desktop
                    - *Desktop Wallpaper*

---

**Prohibit Changing Wallpaper**

- User Configuration
    - Policies
        - Administrative Templates
            - Desktop
                - Desktop
                    - *Prohibit changes*

--- 

**Open Internet Explorer at Login**

- User Configuration
    - Policies
        - Administrative Templates
            - System
                - Logon
                    - *Run these programs at user logon*
                        - *iexplore.exe*

---

**Open Specific Page in IE When It Starts**

- User Configuration
    - Policies
        - Administrative Templates
            - Windows Components
                - Internet Explorer
                    - *Disable changing home page settings*

---

**No Access To .exe**

- User Configuration
    - Policies
        - Windows Settings
            - Security Settings
                - Software Restriction Policies
                    - Additional Rules
                        - *New Path Rule (Right Click)*

---

**Popup After Login**

- User Configuration
    - Policies
        - Windows Settings
            - Scripts (Logon/Logoff)
                - Logon
                    - *Properties (Right Click) -> Add*